
@echo off

SET BUFFER_SIZE=1
SET N_ITER=8
SET RUN_DIR=saved_models\TEST
SET PATH_DATASET_CSV="data\csvs\test_run.csv"
SET GPU_IDS="-1"
SET TORCH_CUDA_ARCH_LIST="8.6"

@REM DEL /S %RUN_DIR%
dir
python train_model.py --n_iter %N_ITER% --path_dataset_csv %PATH_DATASET_CSV% --path_dataset_val_csv %PATH_DATASET_CSV% --interval_save 6 --buffer_size %BUFFER_SIZE% --path_run_dir %RUN_DIR% --gpu_ids %GPU_IDS%
